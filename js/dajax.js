// JavaScript Document
//prototipo que sirve para tratar la respuesta:
String.prototype.tratarResponseText=function(){
    var pat=/<script[^>]*>([\S\s]*?)<\/script[^>]*>/ig;
    var pat2=/\b\s+src=[^>\s]+\b/g;
    var elementos = this.match(pat) || [];
    for(i=0;i<elementos.length;i++) {
        var nuevoScript = document.createElement('script');
        nuevoScript.type = 'text/javascript';
        var tienesrc=elementos[i].match(pat2) || [];
        if(tienesrc.length){
            nuevoScript.src=tienesrc[0].split("'").join('').split('"').join('').split('src=').join('').split(' ').join('');
        }else{
            var elemento = elementos[i].replace(pat,'$1');
            nuevoScript.text = elemento;
        }
        document.getElementsByTagName('body')[0].appendChild(nuevoScript);
    }
    return this.replace(pat,'');
} 
//---------------------------------------------
function creaAjax(){
  var objetoAjax=false;
  try {
   /*Para navegadores distintos a internet explorer*/
   objetoAjax = new ActiveXObject("Msxml2.XMLHTTP");
  } catch (e) {
   try {
     /*Para explorer*/
     objetoAjax = new ActiveXObject("Microsoft.XMLHTTP");
     } 
     catch (E) {
     objetoAjax = false;
   }
  }

  if (!objetoAjax && typeof XMLHttpRequest!='undefined') {
   objetoAjax = new XMLHttpRequest();
  }
  return objetoAjax;
}

function recibir (url,capa,valores,metodo){	FAjax (url,capa,valores,metodo,1);}
//---------------------------------------------
function FAjax (url,capa,valores,metodo,entero)
{
   var ajax=creaAjax();
   var capaContenedora = document.getElementById(capa);

/*Creamos y ejecutamos la instancia si el metodo elegido es POST*/
 if(metodo.toUpperCase()=='POST'){
       
    ajax.open ('POST', url, true);
    ajax.onreadystatechange = function() {
         if (ajax.readyState==1) {
			 
			     if(entero==1)
                 capaContenedora.innerHTML="<span class='e8b'></span><img src='img/ld.gif'/>&nbsp;&nbsp;&nbsp;&nbsp;";
				 
         }
         else if (ajax.readyState==4){
            if(ajax.status==200)
            {  document.getElementById(capa).innerHTML=ajax.responseText.tratarResponseText(); 
            }
            else if(ajax.status==404)
                 {

                     capaContenedora.innerHTML = "La direccion no existe";
                 }
             else
                 {
                     capaContenedora.innerHTML = "Error: ".ajax.status;
                 }
        }
    }
//ajax.setRequestHeader("Content-Type","multipart/form-data");
ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send(valores);
    return;
    }
}
//---------------------------------------------
function datos(d,s,capas)
{
	// Obtendo la capa donde se muestran las respuestas del servidor
	var capa=document.getElementById(capas);
	// Creo el objeto AJAX
	var ajax=creaAjax();
	// Coloco el mensaje "Cargando..." en la capa
	capa.innerHTML="<img src='img/ld.gif'/>&nbsp;&nbsp;&nbsp;&nbsp;";
	// Abro la conexi�n, env�o cabeceras correspondientes al uso de POST y env�o los datos con el m�todo send del objeto AJAX
	ajax.open("POST",""+capas+".php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("p="+d);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)
		{
			// Respuesta recibida. Coloco el texto plano en la capa correspondiente
			capa.innerHTML=ajax.responseText;
		}
	}
}
//---------------------------------------------
function cases(s)
{
	// Obtendo la capa donde se muestran las respuestas del servidor
	var capa=document.getElementById("case");
	// Creo el objeto AJAX
	var ajax=creaAjax();
	// Coloco el mensaje "Cargando..." en la capa
	capa.innerHTML="---<img src='img/ld.gif'/>&nbsp;&nbsp;&nbsp;&nbsp;";
	// Abro la conexi�n, env�o cabeceras correspondientes al uso de POST y env�o los datos con el m�todo send del objeto AJAX
	ajax.open("POST","cases.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("sec="+s);
	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)
		{
			// Respuesta recibida. Coloco el texto plano en la capa correspondiente
			capa.innerHTML=ajax.responseText;
		}
	}
}
//---------------------------------------------
function traerDatos(cap,dato)
{
	// Obtendo la capa donde se muestran las respuestas del servidor
	var capa=document.getElementById(cap);
	// Creo el objeto AJAX
	var ajax=creaAjax();
	// Coloco el mensaje "Cargando..." en la capa
	capa.innerHTML="iiiiii<img src='img/ld.gif'/>...";
	// Abro la conexi�n, env�o cabeceras correspondientes al uso de POST y env�o los datos con el m�todo send del objeto AJAX
	ajax.open("POST","ic/v131.php", true);
	ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	ajax.send("d="+dato);

	ajax.onreadystatechange=function()
	{
		if (ajax.readyState==4)
		{	// Respuesta recibida. Coloco el texto plano en la capa correspondiente
			//capa.innerHTML=ajax.responseText;
			cap.innerHTML="hhhhhhhhhhi";
		}
	}
	return;
}