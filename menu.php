<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//ES" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <html lang="es">
        <?php
        /*error_reporting(E_ALL);
        ini_set('display_errors', '1');*/
        ?>
        <head>
            <!--Import Google Icon Font-->
            <link href="css/icon.css" rel="stylesheet">
                <!--Import materialize.css-->
                <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

                <!--Let browser know website is optimized for mobile-->
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                <title>Escuela - Menu</title>

        </head>
        <nav>
            <ul id="dropdown1" class="dropdown-content">
                <li><a href="menu.php?sel=cre_usu">Crear Usuario</a></li>
                <li><a href="menu.php?sel=list_usu">Listar Usuarios</a></li>
                
            </ul>
            <div class="nav-wrapper blue darken-1">
                <a href="#!" class="brand-logo">Logo</a>
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Usuarios<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a href="menu.php?sel=arch">Archivo</a></li>
                    <li><a href="menu.php?sel=adm">Administrativo</a></li>


                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <li><a href="menu.php?sel=cre_usu">Crear Usuario</a></li>
                    <li><a href="menu.php?sel=list_usu">Listar Usuarios</a></li>
                    <li><a href="menu.php?sel=arch">Archivo</a></li>
                    <li><a href="menu.php?sel=adm">Administrativo</a></li>
                </ul>
            </div>
        </nav>
        <body bgcolor="#e1f5fe">
            <div class="container col  m3 center z-depth-3  ">
                <?php
                $sel = $_GET['sel'];
                switch ($sel) {
                    case 'cre_usu': include("crearUsuarios.php");
                        break;
                    case 'list_usu': include("listarUsuarios.php");
                        break;
                    case 'arch': include("archivos.php");
                        break;
                    case 'adm': include("administrativo.php");
                        break;
                    case 'sub': include("subirNotas.php");
                        break;
                     case 'asig_pro': include("asignarProfesor.php");
                        break;   
                    case 'add_materia': include("agregarMaterias.php");
                        break; 
                    case 'add_semestre': include("agregarSemestres.php");
                        break;
                }
                ?>   
            </div>
        </body>
        <footer class="page-footer  blue lighten-2">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Escuela Misionera del Momiviento Misionero Mundial</h5>
                <p class="grey-text text-lighten-4">You can use rows and columns here to organize your footer content.</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Contacto</h5>
                <ul>
                  <li><a class="grey-text text-lighten-3" >02663213356</a></li>
                  <li><a class="grey-text text-lighten-3" href="http://www.twitter.com/">Twitter</a></li>
                  
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2015 Copyright Alfredo Perozo
            </div>
          </div>
        </footer>
            
    </html>
    <script type="text/javascript" src="js/min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/dajax.js"></script>
    <script>
        $(document).ready(function() {
            $(".button-collapse").sideNav();
            $(".dropdown-button").dropdown();
            $('select').material_select();
        })
    </script>

