<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <!--Import Google Icon Font-->
        <link href="css/icon.css" rel="stylesheet">
            <!--Import materialize.css-->
            <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

            <!--Let browser know website is optimized for mobile-->
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<?php if(!$_POST){?>
<body class="grey lighten-4">
  <div class="container">
  <form id="contact_form" method="POST" enctype="multipart/form-data">
      <div class="row">
        <div class="col s12 m12 offset-m3 l6 offset-l3">
         <div class="card-panel z-depth-2">
          <div class="row">
            <div class="input-field col s12">
             <input id="usuario"  class="input" name="usuario" type="text" value="" size="30" /><br />
              <label for="usuario" data-error="Error" data-success="Correcto">Usuario</label>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">
              <input id="contrasena" name="contrasena"type="password" class="input">
              <label for="password">Contraseña</label>
            </div>
          </div>

          <div class="row">
            <div class="input-field col s12">                
              <button class="btn waves-effect light-blue darken-2 btn-large" type="submit" >Entrar</button>
            </div>            
          </div>
          <p class="flow-text center">¿Olvido su contraseña?</p>
        </div>
      </div>
    </div>
  </form>
</div>

<!--  Scripts-->

<script type="text/javascript" src="js/min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
</body>
<?php }else{
$usuario=$_POST['usuario'];
$contrasena=$_POST['contrasena'];
if($usuario=='admin'&&$contrasena=='1234'){
    echo '<script>location.href="menu.php?sel=";</script>';
}

} ?>
</html>
