<?php
include("config/BaseDatos.php");
$id = $_GET['cod'];
$conexs = new BaseDatos();
$conexs->conectar($conexs->servidor, $conexs->usuario, $conexs->password, $conexs->BD);


if (!$_POST) {
    ?><!--  BEGIN FORM -->
    <body>
        <form method="post" action="">
            <table width="100%" border="1">
                <tr>
                    <td><center> <h3>Creacion de Usuario</h3></center></td>
                </tr>
                <tr>
                    <td><div class="row ">
                            <div class="input-field col s3   ">
                                <select id="tipo_usuario"  name="tipo_usuario" required>
                                    <option value="" disabled selected>Seleccione un tipo de Usuario</option>
                                    <?php
                                    $res = $conexs->sentencia("select * from tipo_usuario");
                                    if ($res) {
                                        $rows = $conexs->numfilas($res);
                                        echo $rows;
                                    }
                                    if ($rows > 0) {
                                        while ($reg = $conexs->filas($res)) {
                                            echo '<option value="' . $reg[0] . '">' . $reg[1] . '</option> ';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div></td>
                </tr>
            </table>




            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">translate</i>
                    <input  id="nombre" name="nombre" type="text" class="validate"  required pattern="^[a-zA-Z]+$">
                    <label for="nombre">Nombre</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">translate</i>
                    <input id="apellido" name="apellido"type="text" class="validate" required pattern="^[a-zA-Z]+$">
                    <label for="apellido">Apellido</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">account_box</i>
                    <input  id="cedula" name="cedula" type="text" class="validate" required pattern="^\d{8}$">
                    <label for="cedula">Cedula</label>
                </div>

                <div class="input-field col s6">
                    <i class="material-icons prefix">call</i>
                    <input id="telefono" name="telefono"type="text" class="validate" pattern="^(\d+)-(\d{7})$">
                    <label for="telefono">Telefono</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">directions</i>
                    <input id="direccion" name="direccion"type="text" class="validate" required>
                    <label for="direccion">Direccion</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">email</i>
                    <input id="correo" name="correo"type="text" class="validate">
                    <label for="correo">Correo</label>
                </div>
            </div>
            <div class="row">
                <center> 
                    <button class="btn waves-effect  blue waves-light" type="submit" name="action">Guardar
                        <i class="material-icons right">save</i>
                    </button>
                    <button class="waves-effect red waves-light btn" type="reset" name="action">Limpiar
                        <i class="material-icons left">clear</i>
                    </button> 
                </center>  
            </div>
            <table width="100%" border="1">

            </table>


        </form>
    </body>
    <?php
} else {
    $tipo_usuario = $_POST["tipo_usuario"];
    $nombre = $_POST["nombre"];
    $apellido = $_POST["apellido"];
    $cedula = $_POST["cedula"];
    $telefono = $_POST["telefono"];
    $direccion = $_POST["direccion"];
    $correo = $_POST["correo"];
    if ($id != null) {
        $res5 = $conexs->sentencia("UPDATE usuario
   SET tipo_usuario_id=" . $tipo_usuario . ", nombre='" . $nombre . "', apellido='" . $apellido . "', cedula='" . $cedula . "', 
       telefono='" . $telefono . "', direccion='" . $direccion . "',  correo='" . $correo . "'
 WHERE usuario_id=" . $id . ";");
        echo '<script language="javascript"> location.href ="?sel=list_usu";</script>';
    } else {
        $res3 = $conexs->sentencia("select * from usuario where cedula like '" . $cedula . "';");
        $reg3 = $conexs->filas($res3);
        if ($res3) {
            $rows3 = $conexs->numfilas($res3);
        }
        if ($rows3 > 0) {
            echo '<script> alert("Esta cedula ya se encuentra registrada");</script>';
        } else {
            $res2 = $conexs->sentencia("INSERT INTO usuario(
            tipo_usuario_id, nombre, apellido, cedula, telefono, 
            direccion, contrasena, estado,correo)
    VALUES ( " . $tipo_usuario . ",'" . $nombre . "', '" . $apellido . "', '" . $cedula . "', '" . $telefono . "', '" . $direccion . "','" . $cedula . "', true,'" . $correo . "'); ");
            $afectadas = pg_affected_rows($res2);
            if ($afectadas > 0) {
                echo "<script> alert('Se ha guardado correctamente');</script>";
            } else {

                echo"<script> alert('Error al guardar');</script>";
            }
            echo '<script language="javascript"> location.href ="?sel=cre_usu";</script>';
        }
    }
}
if ($id != null) {
    $res4 = $conexs->sentencia("select tipo_usuario_id,cedula,apellido,nombre,telefono,direccion,usuario.estado,correo from usuario 
                        where usuario_id =" . $id . ";");
    $reg4 = $conexs->filas($res4);
    if ($res4) {
        $rows4 = $conexs->numfilas($res4);
    }
    if ($rows4 > 0) {
        echo"<script>
        var element = document.getElementById('tipo_usuario');
        element.value =" . $reg4[0] . "; </script>";
        echo "<script>document.getElementById('tipo_usuario').value='$reg4[0]';</script>";
        echo "<script>document.getElementById('cedula').value='$reg4[1]';</script>";
        echo "<script>document.getElementById('apellido').value='$reg4[2]';</script>";
        echo "<script>document.getElementById('nombre').value='$reg4[3]';</script>";
        echo "<script>document.getElementById('telefono').value='$reg4[4]';</script>";
        echo "<script>document.getElementById('direccion').value='$reg4[5]';</script>";
        echo "<script>document.getElementById('correo').value='$reg4[6]';</script>";
    }
}
?>
