<?php
include("config/BaseDatos.php");
$conexs = new BaseDatos();
$conexs->conectar($conexs->servidor, $conexs->usuario, $conexs->password, $conexs->BD);
if (!$_POST) {
    ?><!--  BEGIN FORM -->
    <body>
        <form method="post" action="">
            <table width="100%" border="1">
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              </tr>
                <tr>
                  <td colspan="3"> <center> <h3>Carga de Notas</h3></center></td>
                </tr>
                <tr>
                    <td width="17%">&nbsp;</td>
                <center>
                    <td width="15%"><div class="input-field  18 col s6">
                            <div class="input-field col s6">
                                <i class="material-icons prefix">account_box</i>
                                <input  id="cedula" name="cedula" type="text" class="validate" required pattern="^\d{8}$">
                                <label for="cedula">Cedula</label>
                            </div>         
                        </div></td></center>
                <td width="16%">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td> <center> 
                    <button class="btn waves-effect  blue waves-light" type="submit" name="action">Buscar
                        <i class="material-icons right">search</i>
                    </button>
                    <button class="waves-effect red waves-light btn" type="reset" name="action">Limpiar
                        <i class="material-icons left">clear</i>
                    </button> 
                </center>  </td>
                <td>&nbsp;</td>
                </tr>
            </table> 
<table width="100%" border="1">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>

    </form>
        <?php
    } else {
        $cedula = $_POST["cedula"];
        $res = $conexs->sentencia("select semestre.descripcion,materia.descripcion, profesor.nombre||' '||profesor.apellido,fecha_fin,calificacion from materia_usuario 
 inner join materia_profesor on materia_profesor.materia_profesor_id=materia_usuario.materia_profesor_id
 inner join usuario alumno on alumno.usuario_id=materia_usuario.usuario_id
 inner join usuario profesor on profesor.usuario_id=materia_profesor.usuario_id
 inner join materia on materia.materia_id=materia_profesor.materia_id
 inner join semestre on semestre.semestre_id=materia.semestre_id
 where materia_usuario.usuario_id=(select usuario_id from usuario where cedula like '" . $cedula . "')
 and materia_usuario.estado=true order by semestre.semestre_id;");
        if ($res) {
            $rows = $conexs->numfilas($res);
        }
        ?>
        <div class="input-field col s12">
            <div class="row">
                <form method="post" action="">
                <table width="100%" border="1">
  <tr>
    <td><center> <h3>Carga de Notas</h3></center></td>
  </tr>
</table>
                    <div class="input-field col s3">
                        <i class="material-icons prefix">account_box</i>
                        <input  id="cedula" name="cedula" type="text" class="validate" value="<?php echo trim($cedula); ?>"required pattern="^\d{8}$">
                        <label for="cedula">Cedula</label>
                    </div>
                    <div class="input-field col s2">
                        <button class="btn waves-effect  blue waves-light" type="submit" name="action">buscar
                        <i class="material-icons right">search</i>
                    </button>
                    </div>
                </form>
            </div>
               
            <table class="highlight centered  bordered" border="1">
                <thead>
                    <tr>
                        <th data-field="id">Semestre</th>
                        <th data-field="id">Materia</th>
                        <th data-field="id">Profesor</th>
                        <th data-field="id">Fecha Finalizacion</th>
                        <th data-field="id">Calificacion</th>
                    </tr>
                </thead>
                <?php
                if ($rows > 0) {
                    echo '<tbody>';
                    while ($reg = $conexs->filas($res)) {
                        echo'
<tr>    
<td>' . $reg[0] . '</td>
<td>' . $reg[1] . '</td> 
<td>' . $reg[2] . '</td>
<td>' . $reg[3] . '</td>
<td>' . $reg[4] . '</td>
</tr>    
';
                    }
                    ?>
                    </tbody>
                </table>
                <?php
            } else {
                ?>
                <tbody>
                <td>No hay datos guardados</td>
            </tbody>
        </table>
    <?php }
    ?>
    </div>
    <form  name="subir" id="subir" method="post" action="">
        <div style="display:none;">
            <input  id="cedula" name="cedula" type="text" class="validate" value="<?php echo $cedula; ?> ">
        </div>
        <div class="row ">
            <div class="input-field col s2 ">
                <select onChange="recibir('metodos.php', 'materiadiv', 'semestre=' + this.value + '&usuario=' +<?php echo $cedula; ?> + '&o=1', 'POST');
                        return false" class="browser-default" id="semestre" name="semestre" required>
                    <option value="" disabled selected>Escoja el Semestre</option>
                    <?php
                    $res2 = $conexs->sentencia("select * from semestre where estado=true");
                    if ($res2) {
                        $rows2 = $conexs->numfilas($res2);
                    }
                    if ($rows2 > 0) {
                        while ($reg2 = $conexs->filas($res2)) {
                            echo '<option value="' . $reg2[0] . '">' . $reg2[1] . '</option> ';
                        }
                    }
                    ?>
                </select>
            </div>

            <div id="materiadiv" class="input-field col s2 " style="display:block;" >

                <select class= "browser-default" required>
                    <option value = "" disabled selected>Escoja una materia</option>
                </select>

            </div>
            <div id="profesordiv" class="input-field col s2 " style="display:block;" >
                <select class= "browser-default" required>
                    <option value = "" disabled selected>Escoja un profesor</option>   
                </select>
            </div>
            <div class="input-field col s2">
                <i class="material-icons prefix">event_available</i>
                <input  id="fecha_fin" name="fecha_fin" type="date" class="datepicker" required>
                <label for="fecha_fin">Fecha</label>
            </div>
            <div class="input-field col s2">
                <i class="material-icons prefix">school</i>
                <input  id="calificacion" name="calificacion" type="text" class="validate" required pattern="^\d{2}$">
                <label for="calificacion">Calificacion</label>
            </div>
            <div class="input-field col s1">
                <a  onclick="recibir('metodos.php', 'algundiv', 'materia=' + document.getElementById('materia').value +
                                '&cedula=' + document.getElementById('cedula').value +
                                '&profesor=' + document.getElementById('profesor').value +
                                '&calificacion=' + document.getElementById('calificacion').value +
                                '&fecha_fin=' + document.getElementById('fecha_fin').value + '&o=3', 'POST');
                        return false" class="btn-floating waves-effect circle blue waves-light" >
                    <i class="material-icons right">check</i>
                </a>
            </div>


        </div>
    </form>

    <div id="algundiv" name="algundiv" class="col s12">

    </div>

    <?php
}
?>


</body>
<script type="text/javascript" src="js/min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script>

                $('.datepicker').pickadate({
                    format: 'yyyy-mm-dd',
                    // Creates a dropdown to control month
                    selectYears: 15 // Creates a dropdown of 15 years to control year
                });
</script>

