<?php
include("config/BaseDatos.php");
$conexs = new BaseDatos();
$conexs->conectar($conexs->servidor, $conexs->usuario, $conexs->password, $conexs->BD);
?>
<body>
<form>
<table width="100%"  border="1">
    
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4"><center> <h3>Creación de Materias</h3></center></td>
  </tr>
  <tr>
    <td width="15%">

    <div class="input-field col s2 ">
            <select id="semestre" name="semestre" required>
                <option value="" disabled selected>Seleccione el Semestre</option>
                <?php
                $res = $conexs->sentencia("select semestre_id,descripcion from semestre where estado=true");
                if ($res) {
                    $rows = $conexs->numfilas($res);
                    echo $rows;
                }
                if ($rows > 0) {
                    while ($reg = $conexs->filas($res)) {
                        echo '<option value="' . $reg[0] . '">' . $reg[1] . '</option> ';
                    }
                }
                ?>
            </select>

        </div>
    <td width="48%"> 
        
                <div class="input-field col s6">
                    <i class="material-icons prefix">translate</i>
                    <input  id="descripcion" name="descripcion" type="text" class="validate"  required >
                    <label for="nombre">Descripción</label>
                </div>
        
    </td>
    <center><td width="8%">
    <div class="input-field col s6">
                    <i class="material-icons prefix">format_list_numbered</i>
                    <input  id="uc" name="uc" type="text" class="validate"  required pattern="^\d++$">
                    <label for="uc">UC</label>
                </div>
      </td>
      <td width="9%"><div class="input-field col s3 ">
    <a onClick="recibir('metodos.php', 'ok', 'semestre=' +  document.getElementById('semestre').value +
                '&descripcion='+ document.getElementById('descripcion').value+
                '&uc='+ document.getElementById('uc').value+'&o=6', 'POST');
                        return false" class="btn-floating waves-effect circle blue waves-light" >
                    <i class="material-icons right">check</i>
                </a>
    </div></td>
    </center>
  
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <tr>
      <td colspan="4"><div id="ok" name="ok">
          </div></td>
  </tr>
</table>
  </form>    
</body>
